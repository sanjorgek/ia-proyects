import java.util.Random;
import java.lang.Math;
import java.util.*;

int alto = (new Random()).nextInt(16)+10;
int ancho = (new Random()).nextInt(16)+10;
int tamano = 15;
int iniY = (new Random()).nextInt(alto);
int iniX = (new Random()).nextInt(ancho);
Tablero laberinto;

void setup(){
  size(ancho*tamano+30, alto*tamano+30);
  laberinto = new Tablero(ancho,alto);
  //creo el lugar de inicio en el laberinto
  laberinto.iniciar(laberinto.celdas[iniY][iniX]);
} 

void draw() {
  int i,j;
  //para colorear las celdas, sin contorno
  for(i=0;i<alto;i++){
    for(j=0;j<ancho;j++){
      if(laberinto.celdas[i][j].visitado){
        fill(0,0,175);
      }
      else  fill(0,175,0);
      noStroke();
      rect(laberinto.celdas[i][j].coorX*tamano+15,laberinto.celdas[i][j].coorY*tamano+15,tamano,tamano);
    } 
  }
  fill(175,0,175);
  noStroke();
  rect(laberinto.celdas[iniY][iniX].coorX*tamano+15,laberinto.celdas[iniY][iniX].coorY*tamano+15,tamano,tamano);
  //para poner las lineas verticales
  for(i=0;i<alto;i++){
    for(j=0;j<(ancho-1);j++){
      if(!laberinto.lineaV[i][j])stroke(0);
      else noStroke();
      line((j+1)*tamano+15,i*tamano+15,(j+1)*tamano+15,(i+1)*tamano+15);
    }
  }
  //para poner las lineas horizontales
  for(i=0;i<ancho;i++){
    for(j=0;j<(alto-1);j++){
      if(!laberinto.lineaH[i][j])stroke(0);
      else noStroke();
      line(i*tamano+15,(j+1)*tamano+15,(i+1)*tamano+15,(j+1)*tamano+15);
    }
  }
  //abusando de que el draw se hace como un while lo uso para iterar mi algoritmo
  laberinto.iterar();
}

//Clase pequeña cuyo unico uso es dar una nocion de celda
class Celda{
 int coorX;
 int coorY;
  
 Celda(int x, int y){
   this.coorX=x;
   this.coorY=y;
 }
}

//Clase grande
/**
  *Esta celda tiene una pila de celdas adyacentes, que son celdas 
  *de la clase mas pequeña para evitar clases recursivas
  */
class CeldaAdy extends Celda{
  boolean visitado;
  Stack<Celda> pila;
 
  CeldaAdy(int x, int y, int alto, int ancho){
     super(x,y);
     visitado = false;
     pila = new Stack();
     //estos solo suceden cuando no esta en alguna de las orillas
     if(x!=0) pila.push(new Celda(x-1,y));
     if(x!=ancho-1) pila.push(new Celda(x+1,y));
     if(y!=0) pila.push(new Celda(x,y-1));
     if(y!=alto-1) pila.push(new Celda(x,y+1));
  } 
}

/**
  *Clase que contiene el tablero y los metodos de backtrack
  */
class Tablero{
 int alto;
 int ancho;
 int tamano;
 boolean[][] lineaV;
 boolean[][] lineaH;
 CeldaAdy[][] celdas;
 Random rnd;
 Stack<CeldaAdy> pila;
 
 /**
  *@param ancho del tablero
  *@param alto del tablero
  */
 Tablero(int ancho, int alto){
   this.alto=alto;
   this.ancho=ancho;
   celdas = new CeldaAdy[alto][ancho];
   //Se crean las celdas en el tablero con pequeñas nociones de su entorno
   //como las dimenciones del tablero para poder determinar sus adyacencias
   for(int i=0;i<alto;i++){
     for(int j=0;j<ancho;j++){
       celdas[i][j]=new CeldaAdy(j,i,alto,ancho);
     }
   }
   //Estos arreglos son solo para dibujar las lineas individualmente
   lineaV = new boolean[alto][ancho-1];
   lineaH = new boolean[ancho][alto-1];
   rnd = new Random();
   pila = new Stack();
 }
 
 /**
 *@param cell Celda donde iniciara el recorrido
 */
 void iniciar(CeldaAdy cell){
   cell.visitado=true;
   pila.push(cell);
 }
 
 /**
 *Siguiendo el procedimiento de backtrack con la pila indicado
 */
 void iterar(){
   CeldaAdy aux;
   CeldaAdy aux2;
   int elem;
   //si la pila es vacia quiere decir que termino de hacer su trabajo
   if(!pila.isEmpty()){
     aux = pila.peek();
     //se revisa la cantidad de adyacencias que tiene la celda en el tope de la pila
     //si no contiene nada se procede a realizar pop!
     if(!aux.pila.isEmpty()){
      //Selecciona una de las adyacencias posibles al azar
       elem = rnd.nextInt(aux.pila.size());
       aux2 = celdas[aux.pila.elementAt(elem).coorY][aux.pila.elementAt(elem).coorX];
       //se quita de adyacencias posibles
       aux.pila.removeElementAt(elem);
       //si ya fue visitado en la sig iteracion se seleccionara otra adyacencia posible
       if(!aux2.visitado){
         aux2.visitado = true;
         //se empuja la nueva celda
         pila.push(aux2);
         //Se calcula la direccion del movimiento con respecto a las coordenadas y se desmarca
         //la linea "pisada"
         if((aux.coorX-aux2.coorX)==0){
           System.out.println("Horizontal "+ aux.coorX+" "+aux.coorY+" "+aux2.coorX+" "+aux2.coorY);
           if(aux.coorY>aux2.coorY) lineaH[aux.coorX][aux2.coorY] = true;
           else lineaH[aux.coorX][aux.coorY] = true;
         }else{
           System.out.println("Vertical  "+ aux.coorX+" "+aux.coorY+" "+aux2.coorX+" "+aux2.coorY);
           if(aux.coorX>aux2.coorX) lineaV[aux.coorY][aux2.coorX] = true;
           else lineaV[aux.coorY][aux.coorX] = true;
         }  
       }
     }else{
       pila.pop();
       System.out.println("POP");
     }
   } 
 }
}
