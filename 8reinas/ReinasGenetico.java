public class ReinasGenetico{
	public static void main(String[] args) {
		int tama = 8;
		double muta = 0.2;
		if(args.length>=1) tama = Integer.parseInt(args[0]);
		if(args.length>=2) muta = ((double)Integer.parseInt(args[1]))/100;
		Poblacion pobl = new Poblacion(60,muta,5,tama);	
		Poblacion pobl2 = new Poblacion(60,muta,5,tama);
		Individuo ind1;
		Individuo ind2;
		Individuo hijo;
		int cota = (int)((tama*(tama-1))/2);
		int cont = 0;
		int cont2 = 5;
		boolean bandera = true;
		while(bandera){
			pobl.asignarFitness();
			pobl2.elitismo(pobl);
			while(cont2<60){
				ind1 = pobl.seleccionRuleta();
				ind2 = pobl.seleccionRuleta();
				hijo = ind1.recombinar(ind2);
				hijo.mutacion();
				pobl2.add(cont2,hijo);
				cont2++;
			}
			cont2 = 5;
			cont++;
			pobl = pobl2;
			System.out.println(pobl.mejorIndividuo());
			if(pobl.mejorIndividuo().getFitness()==cota) bandera=false;
			if(cont==10000) bandera = false;
		}
		pobl.asignarFitness();
		Individuo mejor = pobl.mejorIndividuo();
		if(cont==10000) System.out.println("Mejor solucion: "+ mejor + " y la cota era: "+cota);
		if(mejor.getFitness()==cota) System.out.println("Solucion optima: "+ mejor + " en la generacion "+cont+" la cota era: "+cota);
	}	
}