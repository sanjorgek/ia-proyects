import java.util.Random;

public class Poblacion{
	protected Individuo[] lista;
	protected int elitismo;
	protected int tam;
	protected int numero;
	public Random rnd = new Random();

	public Poblacion(int num,double mutacion,int elitismo, int tamano){
		lista = new Individuo[num];
		this.elitismo = elitismo;
		tam = tamano;
		for(int i=0;i<num;i++) lista[i] = new Individuo(tam,mutacion);
		numero = num;
	}

	public void asignarFitness(){
		for(Individuo t: lista){
			t.setFitness();
		}
	}

	public int sumaFitness(){
		int suma = 0;
		for(Individuo t: lista) suma+=t.getFitness();
		return suma;
	}

	public void add(int n, Individuo ind){
		lista[n] = ind;
	}

	public int elitismo(Poblacion pop){
		int cont=0;
		for(int i=pop.lista[0].getNorm();i>=0;i--){
			for(Individuo t: pop.lista){
				if(cont==elitismo) return 0;
				if(t.getFitness()==i){
					add(cont, t);
					cont++;
				}
			}
		}
		return 1;
	}

	public Individuo seleccionRuleta(){
		int sum = sumaFitness();
		while(true){
			for(Individuo t: lista){
				double in = rnd.nextDouble();
				if(in<=((double)t.getFitness()/(double)sum)) return t;
			}
		}
	}

	public Individuo getIndividuo(int i){
		return lista[i];
	}

	public Individuo mejorIndividuo(){
		for(int i=lista[0].getNorm();i>=0;i--){
			for(Individuo t: lista){
				if(t.getFitness()==i) return t;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		Poblacion prueba1 = new Poblacion(15,0.2,3,8);
		Poblacion prueba2 = new Poblacion(15,0.2,3,8);
		prueba1.asignarFitness();
		System.out.println("Poblacion 1");
		for(Individuo t: prueba1.lista)System.out.println(t);
		prueba2.elitismo(prueba1);
		prueba2.asignarFitness();
		System.out.println("Poblacion 2");
		for(Individuo t: prueba2.lista)System.out.println(t);
		Individuo persona = prueba2.seleccionRuleta();
		System.out.println("seleccion de Ruleta");
		System.out.println(persona);
		System.out.println("Mejor individuo");
		System.out.println(prueba2.mejorIndividuo());
	}
}