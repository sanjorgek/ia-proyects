import java.util.Random;

public class Individuo{
	protected int[] casillas;
	protected int fitness;
	protected int norm;
	protected double muta;
	public Random ran = new Random();

	public Individuo(int n,double mut){
		fitness = 0;
		casillas = new int[n];
		muta = mut;
		for(int i=0;i<n;i++) casillas[i] = ran.nextInt(n)+1;
		norm = (int)((n*(n-1))/2);
	}

	public Individuo(int n, double mut, int[] p1, int[] p2){
		fitness = 0;
		casillas = new int[n];
		muta = mut;
		int i;
		for(i=0;i<p1.length;i++) casillas[i] = p1[i];
		for(i=0;i<p2.length;i++) casillas[i+p1.length] = p2[i];		
		norm = (int)((n*(n-1))/2);
	}

	public int numAtaques(){
		int salida=0;
		int aux = 0;
		for(int i=0;i<casillas.length-1;i++){
			for(int j=i+1; j<casillas.length;j++){
				aux = j-i;
				if(casillas[i]==casillas[j]) salida++;
				if(casillas[i]==(casillas[j]+aux)) salida++;
				if(casillas[i]==(casillas[j]-aux)) salida++;
			}
		}
		return salida;
	}

	/**
	 *da un arreglo que es que contiene n elementos de un arreglo de m
	 */
	public int[] subE(int n){
		int[] aux = new int[n];
		for(int i=0; i<n; i++) aux[i] = casillas[i];
		return aux;
	}

	/**
	 *da un arreglo que es que contiene m-n elementos de un arreglo de m
	 */	
	public int[] subI(int n){
		int[] aux = new int[casillas.length-n];
		for(int i=n; i<casillas.length; i++) aux[i-n] = casillas[i];
		return aux;
	}

	public void setFitness(){
		fitness = norm-this.numAtaques();
	}

	public void mutacion(){
		for(int i=0; i<casillas.length; i++) if(ran.nextDouble()<=muta) casillas[i]=ran.nextInt(casillas.length)+1;		
	}

	public Individuo recombinar(Individuo ind){
		int corte = ran.nextInt(casillas.length)+1;
		Individuo aux = new Individuo(casillas.length, muta,subE(corte),ind.subI(corte));
		return aux;
	}

	public int getFitness(){
		return fitness;
	}

	public int getNorm(){
		return norm;
	}

	public String toString(){
		String cadena = "[";
		for(int i = 0; i<casillas.length; i++) cadena = cadena + " " + casillas[i];
		return cadena+" ]\t Su fitness es (" + getFitness()+")";
	}

	public static void main(String[] args) {
		Individuo prueba1 = new Individuo(8,0.2);
		prueba1.setFitness();
		Individuo prueba2 = new Individuo(8,0.2);
		prueba2.setFitness();
		System.out.println("Primer ind\t"+prueba1);
		System.out.println("segundo ind\t"+prueba2);
		prueba2.mutacion();
		System.out.println("Mutacion\t"+prueba2);
		Individuo prueba3 = prueba2.recombinar(prueba1);
		prueba3.setFitness();
		System.out.println("Recombinacion\t"+prueba3);
	}
}