Programas y tareas de IA
========================

Este git contiene todos los programas realizados para la clase de IA de la
carrera de Ciencias de la Computacion Plan 2013 UNAM

Las carpetas de *Termitas* y *Laberinto* incluyen unos archivos hechos en java con la ayuda del
IDE **Processing2** para trabajarlas.

La carpeta *Gato* solo implementa de manera torpe DeepSearch en un arbol de posibles jugadas de 
un tablero de gato.

La carpeta *GatoM* implementa sobre la clase anterior (con unas pequeñas modificaciones) el algoritmo
MiniMax para encontrar la sig jugada a realizar.
