/**
 *Clase para generar el tablero del juego del gato.
 *El cual representaremos en un arreglo lineal dado que es mas facil y barato.
 */
class Espacio{
	/**
	 *Arreglo para contener las jugadas.
	 */
	protected int[] casillas;

	/**
	 *Crea el Espacio inicial
	 */
	public Espacio(){
		casillas = new int[9];
	}
	/**
	 *Crea el Espacio dado un Espacio de gato
	 */
	public Espacio(int[] x, int[] o){
		casillas = new int[9];
		int i;
		for(i=0; i<x.length; i++) casillas[x[i]] = 1;
		for(i=0; i<o.length; i++) casillas[o[i]] = 2;
	}
	/**
	 *Crea un Espacio apartir de un Espacio previo y una jugada
	 *@param anterior Espacio previo de la partida
	 *@param j Bandera para identificar al jugador que realiza la jugada
	 *@param n posicion de la jugada
	 */
	public Espacio(Espacio anterior, boolean j, int n){
		casillas = new int[9];
		for(int i=0; i<9; i++) casillas[i] = anterior.getEspacio(i);
		if(j) casillas[n]=1;
		else casillas[n]=2;
	}

	/**
	 *Crea un Espacio apartir de un Espacio previo
	 *@param anterior Espacio previo de la partida
	 */
	public Espacio(Espacio anterior){
		casillas = new int[9];
		for(int i=0; i<9; i++) casillas[i] = anterior.getEspacio(i);
	}

	/**
	 *Regresa el Espacio de una posicion en el tablero
	 *@param n ubicacion en el arreglo lineal
	 *@return 0 si esta vacio, 1 si el jugador uno tiro y 2 si el jugador dos tiro en esa posicion
	 */
	public int getEspacio(int n){
		return casillas[n];
	}


	/**
	 *Verifica si aun hay lugar en el espacio para tirar
	 */
	public boolean hayEspacio(){
		for(int i=0; i<9; i++) if(casillas[i]==0)return true;
		return false;
	}

	/**
	 *Metodo para representar en la terminal al tablero
	 */
	public String toString(){
		String aux = "";
		for(int i=0; i<9; i++){
			if(casillas[i]==2)aux+="O";
			if(casillas[i]==1)aux+="X";
			if(casillas[i]==0) aux+=" ";
			if(i%3==2) aux+="\n";
		}
		return aux;
	}

	public static void main(String[] args) {
		Espacio prueba = new Espacio();
		System.out.print(prueba.toString());
		int[] xS = {5,4,3,6};
		int[] oS = {1,8,2};
		prueba = new Espacio(xS,oS);
		System.out.print(prueba.toString());
		Espacio prueba2 = new Espacio(prueba, false, 7);
		System.out.print(prueba2.toString());

	}
}