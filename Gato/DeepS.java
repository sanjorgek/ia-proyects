public class DeepS{
	public static TripleC contar(Gato estadosG){
		TripleC cont = new TripleC();
		if(estadosG.estadoFinal()){
			if(estadosG.estadoFinal(1))cont.sumarTripleC(1,0,0);
			if(estadosG.estadoFinal(2))cont.sumarTripleC(0,1,0);
			if((!estadosG.gatoE.hayEspacio())&&(!estadosG.estadoFinal(2))&&(!estadosG.estadoFinal(1)))cont.sumarTripleC(0,0,1);
		}else{
			int tam = estadosG.getNPosibles();
			for(int i=0; i<tam; i++){
				cont.sumarTripleC(DeepS.contar(estadosG.getPosible(i)));
			}
		}
		return cont;
	}

	public static void main(String[] args) {
		int[] xS = {5,8};
		int[] oS = {1};
		Espacio prueba = new Espacio(xS,oS);
		Gato cat = new Gato(prueba);
		cat = Gato.generadora(cat,false);
		TripleC contF = DeepS.contar(cat);
		System.out.println(cat + " " +contF);
		cat = Gato.generadora(new Gato(),true);
		contF = DeepS.contar(cat);
		System.out.println(cat + " " +contF);
	}
}