public class TripleC{
	protected int g;
	protected int p;
	protected int e;

	public TripleC(){
		g=0;
		p=0;
		e=0;
	}

	public int getG(){
		return g;
	}

	public int getP(){
		return p;
	}

	public int getE(){
		return e;
	}

	public void sumarTripleC(TripleC cont){
		this.g+=cont.getG();
		this.p+=cont.getP();
		this.e+=cont.getE();
	}

	public void sumarTripleC(int g,int p, int e){
		this.g+=g;
		this.p+=p;
		this.e+=e;
	}

	public String toString(){
		return "(x="+getG()+",o="+getP()+",e="+getE()+")";
	}
}