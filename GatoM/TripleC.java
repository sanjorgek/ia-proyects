/**
 *Clase auxiliar para la clase DeepS
 */
public class TripleC{
	protected int g;
	protected int p;
	protected int e;

	/**
	 *Constructor vacio
	 */
	public TripleC(){
		g=0;
		p=0;
		e=0;
	}

	/**
	 *@return Da el valor de la primera entrada
	 */
	public int getG(){
		return g;
	}

	/**
	 *@return Da el valor de la segunda entrada
	 */
	public int getP(){
		return p;
	}

	/**
	 *@return Da el valor de la tercera entrada
	 */
	public int getE(){
		return e;
	}

	/**
	 *Suma un TripleC con otro TripleC
	 *@param cont sumando2
	 */
	public void sumarTripleC(TripleC cont){
		this.g+=cont.getG();
		this.p+=cont.getP();
		this.e+=cont.getE();
	}

	/**
	 *Suma un TripleC con unas entradas, los suma en sus entradas
	 *respectivas
	 */
	public void sumarTripleC(int g,int p, int e){
		this.g+=g;
		this.p+=p;
		this.e+=e;
	}

	public String toString(){
		return "(x="+getG()+",o="+getP()+",e="+getE()+")";
	}
}