class TripleG{
	protected int calf;
	protected Gato tab;
	protected int prof;

	public TripleG(int calf, Gato cat, int prof){
		this.calf = calf;
		this.tab = new Gato(cat.getGato());
		this.prof = prof;
	}

	public TripleG(TripleG cont){
		this.calf = cont.getCalf();
		this.tab = new Gato(cont.getTab().getGato());
		this.prof = cont.prof;
	}

	public int getCalf(){
		return calf;
	}

	public Gato getTab(){
		return tab;
	}

	public int getProf(){
		return prof;
	}
}

public class MiniMax{
	public static int calificar(Gato cat){
		if(cat.estadoFinal(1)) return 1;
		if(cat.estadoFinal(2)) return -1;
		return 0;
	}

	public static boolean esMax(Gato cat){
		return cat.getTurno();
	}

	public static int miniMax(Gato cat){
		if(cat.estadoFinal())return calificar(cat);
		int value;
		int aux;
		if(esMax(cat)){
			cat = Gato.generarSucesores(cat,true);
			value = miniMax(cat.getPosible(0));
			for(int i=1;i<cat.getNPosibles();i++){
				aux = miniMax(cat.getPosible(i));
				value = (aux>value)? aux:value;
			}
			return value;
		}else{
			cat = Gato.generarSucesores(cat,false);
			value = miniMax(cat.getPosible(0));
			for(int i=1;i<cat.getNPosibles();i++){
				aux = miniMax(cat.getPosible(i));
				value = (aux<value)? aux:value;
			}
			return value;
		}
	}

	public static TripleG miniMaxE(Gato cat, int prof){
		if(cat.estadoFinal())
			return new TripleG(calificar(cat),cat,prof);
		TripleG value;
		TripleG aux;
		if(esMax(cat)){
			cat = Gato.generarSucesores(cat,true);
			value = new TripleG(miniMaxE(cat.getPosible(0),prof+1).getCalf(),cat.getPosible(0), prof);
			for(int i=1;i<cat.getNPosibles();i++){
				aux = new TripleG(miniMaxE(cat.getPosible(i),prof+1).getCalf(),cat.getPosible(i), prof);
				if(aux.getCalf()>=value.getCalf()){
					value = new TripleG(aux.getCalf(), aux.getTab(), prof);
					if(aux.getProf()<value.getProf())
						value = new TripleG(aux.getCalf(), aux.getTab(), prof);
				}
			}
			return value;
		}else{
			cat = Gato.generarSucesores(cat,false);
			value = new TripleG(miniMaxE(cat.getPosible(0),prof+1).getCalf(),cat.getPosible(0), prof);
			for(int i=1;i<cat.getNPosibles();i++){
				aux = new TripleG(miniMaxE(cat.getPosible(i),prof+1).getCalf(),cat.getPosible(i), prof);
				if(aux.getCalf()<=value.getCalf()){
					value = new TripleG(aux.getCalf(), aux.getTab(), prof);
					if(aux.getProf()<value.getProf())
						value = new TripleG(aux.getCalf(), aux.getTab(), prof);
				}
			}
			return value;
		}
	}

	public static void main(String[] args) {
		Espacio prueba;
		Gato cat;
		int[] oS = {1,4,5};
		int[] xS = {2,3,8};
		prueba = new Espacio(xS,oS);
		System.out.print(prueba.toString());
		cat = new Gato(prueba);
		if(cat.estadoFinal()) System.out.println("fin");
		else System.out.println("no es fin");
		System.out.println(miniMaxE(cat,0).getTab());
		int[] oS1 = {5,7};
		int[] xS1 = {2,6,8};
		prueba = new Espacio(xS1,oS1);
		System.out.print(prueba.toString());
		cat = new Gato(prueba);
		if(cat.estadoFinal()) System.out.println("fin");
		else System.out.println("no es fin");
		System.out.println(miniMaxE(cat,0).getTab());
		int[] oS2 = {0,1,6};
		int[] xS2 = {2,5,7};
		prueba = new Espacio(xS2,oS2);
		System.out.print(prueba.toString());
		cat = new Gato(prueba);
		if(cat.estadoFinal()) System.out.println("fin");
		else System.out.println("no es fin");
		System.out.println(miniMaxE(cat,0).getTab());

	}
}
