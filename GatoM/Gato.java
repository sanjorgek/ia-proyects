import java.util.*;

/**
 *Clase para crear el arbol de tableros de gato
 */
public class Gato{
	protected Espacio gatoE;
	protected Stack<Gato> posiblesMov;

	/**
	 *Creo el estado de gato vacio.
	 */
	public Gato(){
		gatoE = new Espacio();
		posiblesMov = new Stack<Gato>();
	}

	/**
	 *Constructor de gato.
	 *@param tablero Espacio desde el cual se empezara a generar
	 */
	public Gato(Espacio tablero){
		gatoE = new Espacio(tablero);
		posiblesMov = new Stack<Gato>();
	}

	/**
	 *@return el Espacio actual
	 */
	public Espacio getGato(){
		return gatoE;
	}

	/**
	 *@return la cantidad de posibles jugadas
	 */
	public int getNPosibles(){
		return posiblesMov.size();
	}

	/**
	 *@param i Posicion en la lista de posibles; lanzara una excepcion si se sobrepasa de la cantidad de elementos.
	 *@return Tablero con movimiento posible
	 */
	public Gato getPosible(int i){
		return posiblesMov.elementAt(i);
	}

	public boolean getTurno(){
		return gatoE.turno();
	}

	/**
	 *Metodo para agregar un nuevo estado
	 *@param est posible movimiento de gato
	 */
	public void pushMov(Gato est){
		posiblesMov.push(est);
	}

	/**
	 *@return True si ya no hay lugares donde tirar o algun jugador gano; en otro caso False
	 */
	public  boolean estadoFinal(){
		if(estadoFinal(1)){
			return true;
		}else{
			if(estadoFinal(2)){
				return true;
			}else{
				if(!gatoE.hayEspacio()){
					return true;
				}else
				return false;
			}
		}
	}

	/**
	 *@param j 1 si es el jugador que tira X y si es 2 es o
	 *@return True si gano; False en otro caso
	 */
	public boolean estadoFinal(int j){
		//horizontal
		if(gatoE.getEspacio(0)==j&&gatoE.getEspacio(1)==j&&gatoE.getEspacio(2)==j) return true;
		if(gatoE.getEspacio(3)==j&&gatoE.getEspacio(4)==j&&gatoE.getEspacio(5)==j) return true;
		if(gatoE.getEspacio(6)==j&&gatoE.getEspacio(7)==j&&gatoE.getEspacio(8)==j) return true;
		//vertical
		if(gatoE.getEspacio(0)==j&&gatoE.getEspacio(3)==j&&gatoE.getEspacio(6)==j) return true;
		if(gatoE.getEspacio(1)==j&&gatoE.getEspacio(4)==j&&gatoE.getEspacio(7)==j) return true;
		if(gatoE.getEspacio(2)==j&&gatoE.getEspacio(5)==j&&gatoE.getEspacio(8)==j) return true;
		//diagonal
		if(gatoE.getEspacio(0)==j&&gatoE.getEspacio(4)==j&&gatoE.getEspacio(8)==j) return true;
		if(gatoE.getEspacio(6)==j&&gatoE.getEspacio(4)==j&&gatoE.getEspacio(2)==j) return true;
		return false;
	}

	/**
	 *@param n casilla a verificar
	 *@return True si la casilla esta disponible, False en otro caso
	 */
	public boolean disponible(int n){
		if(gatoE.getEspacio(n)==1) return false;
		if(gatoE.getEspacio(n)==2) return false;
		return true;
	}

	/**
	 *Funcion generadora de Espacios posibles
	 *@param ant Gato proporcionado
	 *@param jugador Bandera para indicar el turno del jugador a tirar
	 *@return Gato con todos los estados posibles
	 */
	public static Gato generadora(Gato ant, boolean jugador){
		Gato aux = new Gato(ant.getGato());
		Gato aux2;
		//si el gato no es un tablero bloqueado se generan los sig
		if(!aux.estadoFinal()){
			for(int i=0; i<9; i++){
				//se verifican las casilla en las que se puede tirar
				if(aux.disponible(i)){
					//se genera la sig jugada posible en el espacio disponible
					aux2 = new Gato(new Espacio(aux.getGato(),jugador,i));
					//se empuja las posibilidades de esta jugada, ahora con la bandera del sig jugador
					aux.pushMov(generadora(aux2,!jugador));
				}
			}
		}
		return aux;
	}

	public static Gato generarSucesores(Gato ant, boolean jugador){
		Gato aux = new Gato(ant.getGato());
		Gato aux2;
		//si el gato no es un tablero bloqueado se generan los sig
		if(!aux.estadoFinal()){
			for(int i=0; i<9; i++){
				//se verifican las casilla en las que se puede tirar
				if(aux.disponible(i)){
					//se genera la sig jugada posible en el espacio disponible
					aux2 = new Gato(new Espacio(aux.getGato(),jugador,i));
					//se empuja las posibilidades de esta jugada, ahora con la bandera del sig jugador
					aux.pushMov(aux2);
				}
			}
		}
		return aux;
	}

	public int getTiroDif(Gato cat){
		for(int i=0;i<9;i++) if(this.gatoE.getEspacio(i)!=cat.gatoE.getEspacio(i)) return i;
		return -1;
	}

	public String toString(){
		return gatoE.toString();
	}

	public static void main(String[] args) {
		Espacio prueba;
		int[] xS = {5,3,6};
		int[] oS = {1,8,2};
		prueba = new Espacio(xS,oS);
		System.out.print(prueba.toString());
		Gato cat;
		cat = new Gato(prueba);
		if(cat.estadoFinal()) System.out.println("fin");
		else System.out.println("no es fin");
		cat = Gato.generadora(cat,true);
		int tam = cat.posiblesMov.size();
		for(int i = 0 ; i<tam;i++){
			System.out.println(cat.posiblesMov.pop());
		}
		Espacio prueba2 = new Espacio();
		System.out.print(prueba2.toString());
		cat = new Gato(prueba2);
		Gato cat2 = Gato.generadora(cat,true);
		System.out.println("acabe");
		System.out.println(cat2);
		tam = cat2.posiblesMov.size();
		for(int i = 0 ; i<tam;i++){
			System.out.println(cat2.posiblesMov.pop());
		}
	}
}