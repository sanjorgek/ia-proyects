%Ejercicio 1
word(astante, a,s,t,a,n,t,e). 
word(astoria, a,s,t,o,r,i,a). 
word(baratto, b,a,r,a,t,t,o). 
word(cobalto, c,o,b,a,l,t,o). 
word(pistola, p,i,s,t,o,l,a). 
word(statale, s,t,a,t,a,l,e).
crossword(V1,V2,V3,H1,H2,H3):-
	word(V1,_,B,_,D,_,F,_),
	word(V2,_,I,_,K,_,M,_),
	word(V3,_,P,_,R,_,T,_),
	word(H1,_,B,_,I,_,P,_),
	word(H2,_,D,_,K,_,R,_),
	word(H3,_,F,_,M,_,T,_).

%Ejercicio 2
subconjunto([],[]).
subconjunto([X|L1],[X|L2]):-subconjunto(L1,L2).
subconjunto(L1,[_|L2]):-subconjunto(L1,L2).

particion(L1,L2):-findall(Y,subconjunto(Y,L1),L2).

suma([],0).
suma([X|L],Y):-suma(L,D),Y is X+D.

pertenece(A,[A|_]).
pertenece(A,[_|C]):-pertenece(A,C).

cantex(0,[]).
%cantex(X,L):-particion(L,PL),pertenece(P,PL),suma(P,X).
cantex(X,L):-subconjunto(LL,L),suma(LL,X).

%Ejercicio 3
ady(r1,r4).
ady(r1,r2).
ady(r1,r3).
ady(r1,r5).
ady(r2,r1).
ady(r2,r3).
ady(r3,r1).
ady(r3,r5).
ady(r4,r1).
ady(r4,r6).
ady(r4,r5).
ady(r5,r1).
ady(r5,r3).
ady(r5,r4).
ady(r6,r4).

color(rojo).
color(verde).
color(amarillo).
color(azul).

adyacente(X,Y):-ady(X,Y);ady(Y,X).

pintar(X,Y,Cx,Cy):-((adyacente(X,Y),color(Cx),color(Cy),Cx\=Cy);(not(adyacente(X,Y)),color(Cx),color(Cy))). 

colorea1(R1,R2,R3,R4,R5,R6):-pintar(R1,R2,C1,C2),pintar(R1,R3,C1,C3),pintar(R1,R4,C1,C4),pintar(R1,R5,C1,C5),pintar(R1,R6,C1,C6),
							pintar(R2,R3,C2,C3),pintar(R2,R4,C2,C4),pintar(R2,R5,C2,C5),pintar(R2,R6,C2,C6),
							pintar(R3,R4,C3,C4),pintar(R3,R5,C3,C6),pintar(R2,R6,C2,C6),
							pintar(R4,R5,C4,C6),pintar(R4,R6,C4,C6),
							pintar(R5,R6,C5,C6).

colorea2(R1,R2,R3,R4,R5,R6,C1,C2,C3,C4,C5,C6):-pintar(R1,R2,C1,C2),pintar(R1,R3,C1,C3),pintar(R1,R4,C1,C4),pintar(R1,R5,C1,C5),pintar(R1,R6,C1,C6),
							pintar(R2,R3,C2,C3),pintar(R2,R4,C2,C4),pintar(R2,R5,C2,C5),pintar(R2,R6,C2,C6),
							pintar(R3,R4,C3,C4),pintar(R3,R5,C3,C6),pintar(R2,R6,C2,C6),
							pintar(R4,R5,C4,C6),pintar(R4,R6,C4,C6),
							pintar(R5,R6,C5,C6).

colorea(C1,C2,C3,C4,C5,C6):-pintar(r1,r2,C1,C2),pintar(r1,r3,C1,C3),pintar(r1,r4,C1,C4),pintar(r1,r5,C1,C5),pintar(r1,r6,C1,C6),
							pintar(r2,r3,C2,C3),pintar(r2,r4,C2,C4),pintar(r2,r5,C2,C5),pintar(r2,r6,C2,C6),
							pintar(r3,r4,C3,C4),pintar(r3,r5,C3,C6),pintar(r3,r6,C2,C6),
							pintar(r4,r5,C4,C6),pintar(r4,r6,C4,C6),
							pintar(r5,r6,C5,C6).